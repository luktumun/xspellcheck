import { useState, useRef } from "react";
import "./styles.css";

export default function App() {
  const [val, setVal] = useState("");
  const ref = useRef();
  const customDictionary = {
    teh: "the",

    wrok: "work",

    fot: "for",

    exampl: "example",
  };

  const arr = Object.values(customDictionary);
  const arr1 = Object.keys(customDictionary);
  function parr(event) {}
  function onChange(event) {
    setVal(event.target.value);
    ref.current = event.target.value;
    var arr3 = [];
    var arr2 = ref.current.split(" ");
    console.log(ref.current, arr2);
    for (let i = 0; i < arr2.length; i++) {
      {
        arr1.map((itm) => {
          if (itm == arr2[i].toLowerCase()) {
            var z = document.createElement("p"); // is a node
            var r = arr1.indexOf(arr2[i].toLowerCase());

            z.innerHTML = "Did you mean: " + arr[r] + "?";

            document.body.appendChild(z);
          }
        });
      }
    }
  }

  return (
    <div className="App">
      <h1>Spell Check and Auto-Correction</h1>
      <textarea
        value={val}
        placeholder="Enter text..."
        onChange={onChange}
        spellCheck={true}
        lang={"en"}
      ></textarea>
    </div>
  );
}
